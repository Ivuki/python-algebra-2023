# Python Algebra 2023

Repozitorij za vecinu kodova koje cemo raditi na satu. Nece biti identicno i satu ce uglavnom biti objasnjeno vise i detaljnije nego li u samim kodovam

## Downlaod - Preuzimanje repozitorija
### Terminal:

1. otvoripustimo vsCode da odradi svoje ti terminal
1. pozicijonirati se u folder u kojem zelimo da budu zadatci
1. upisati `git clone https://gitlab.com/Ivuki/python-algebra-2023.git` (ovaj korat traje malo dulje)
1. perijodicki se pozicijonirati u taj folder u terminalu i pokrenuti `git pull`kako bi se update-ali zadatci

### vsCode:

1. otvoriti link `https://gitlab.com/Ivuki/python-algebra-2023`
1. kliknuti na gumb na kojem pise "clone"
1. odabrati "Visual studio code (HTTPS)"
1. kada se vsCode otvori odabrati folder u kojem zelimo da budu zadatci (pustimo vsCode da odradi svoje)
1. perijodicki se pozicijonirati u dani folder i pokrenuti `git pull`

Ako nekome treba pomoc najjednostavnije je pitati na discordu pa cemo vam ja ili jedan od kolega koji zna o cemu je rijec pomoci

## Osnove programiranja u pythonu
U ovom modulu radit cemo osnovno programiranje u pythonu

1. ### Varijable
Tu se nalaze svi zadatci koje smo prosli tokom dijela u kojem se obraduju varijable.

- #### Jednostavni_tipov
Jednostavni tipovi podataka kao sto su brojevi (int i float), bool i String. String nije jednstavni tip podatka, ali pobzirom na to kada i kako se obraduje je tu grupiran.

- #### Liste
Slozena struktrura u kojoj podatke spremamo i dohvacamo slijedno (pomocu indexa). 
Dlupikati podataka su moguci.
Promijena podataka ili brisanje istih je moguce.

- #### Rijecnici
Slozena struktura u kojoj se podatci spremaju i dohvacaju preko pojedinog kljuca. 
Kljuc se ne moze duplicirati, ali vrijednosti na kljucu mogu.
Promjena podataka ili brisanje istih je moguca.

- #### Ostale napredne strukture
Ovdije su navedeni i objasnjeni setovi, kao strktura koja nema ponavljajucih elemenata.
I tuple, kao struktura koja se ne mjenja jednom kad je postavljenja.
