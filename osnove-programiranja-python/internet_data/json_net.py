import requests
import json

URL  = 'https://jsonplaceholder.typicode.com/todos'

response = requests.get(URL)
#GET -> dohvati podatke
#POST -> dodaj novi
#PUT -> izmijeni postojeci
#PATCH -> izmijeni dio postojeceg
#DELETE -> obrisi element

# status codes:
# 2xx -> sve je proslo ok
# 3xx -> redirekcija
# 4xx -> Frontend je zeznuo
# 5xx -> Backend je zeznuo
if response.status_code == 200:
    todos = response.json()

    for todo in todos:
        print(f"Titile for todo {todo['id']} is {todo['title']}")

    with open('file.json', 'w') as file:
        json.dump(todos, file)
else:
    print("Error code:", response.status_code)