import requests
from bs4 import BeautifulSoup

# Napravimo funkciju koja ce bratiti broj zvjezdica ovisno o nazivu ocjene
def get_ocjena(tag):
    for naziv, broj_zvjezdica in opis_ocjena.items():
        if naziv in tag["class"]:
            return broj_zvjezdica

# Kreirajmo varijable koje ce nam pomoci za lakse kodiranje
opis_ocjena = {
	"One":   "*",
	"Two":   "* *",
	"Three": "* * *",
	"Four":  "* * * *",
	"Five":  "* * * * *"
}
cijena_selector = ".price_color"
naslov_selector = ".product_pod h3 a"
ocjena_selector = ".star-rating"


# Dohvatimo podatke s interneta
sirovi_podaci = requests.get("http://books.toscrape.com/").content
# Pretvorimo ih u "citljivi" skup HTML tagova
sadrzaj = BeautifulSoup(sirovi_podaci, "html.parser")

# Pomocu .select() metode dohvatimo SVE html tagove koji imaju CSS class imena ".price_color".
# To ponovimo za druga dva tipa koje zelimo dohvatiti
cijene = sadrzaj.select(cijena_selector)
naslovi = sadrzaj.select(naslov_selector)
ocjene = sadrzaj.select(ocjena_selector)

# Prodimo kroz gore kreirane liste i njihov sadrzaj pohranimo u tekstualnu datoteku.
# Ova datotek ima nastavak .csv, ali to je datoteka identicna kao i TXT. 
# Jedino ovako je zgodno sto se kasnije moze otvoriti u Excelu ili Pandas Python biblioteci za rad s tabelama.
with open("books.csv", "w", encoding="utf-8") as file_writer:
    # zip() je funkcija koja kao parametar prima vise kolekcija, zatim iz svake vraca jedan po jedan element
    # u prethodno definirane varijable. Zamjena za vise FOR petlji
	for cijena, naslov, ocjena in zip(cijene, naslovi, ocjene):
		file_writer.write(f"{naslov['title']};{cijena.string};{get_ocjena(ocjena)}\n")