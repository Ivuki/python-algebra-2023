from models.book import Book
from models.author import Author
from models.publisher import Publisher
from sqlalchemy.orm import sessionmaker

class BookService:
    def __init__(self, engine):
        Session = sessionmaker(bind=engine)
        self.session = Session()

    def create_book(self, name, price, is_available, author_id, publisher_id):
        book = Book(name=name, price=price, is_available=is_available,
                    author_id=author_id, publisher_id=publisher_id)
        self.session.add(book)
        self.session.commit()

    def get_all_books(self):
        return self.session.query(Book).all()

    def get_book_by_id(self, book_id):
        return self.session.query(Book).filter(Book.id == book_id).first()

    # Implement other book-related methods as needed
