from .book.add_book_screen import AddBookScreen
from .book.show_book_screen import ShowBookScreen
from .publisher.show_publishers_screen import ShowPublisherScreen
from .publisher.add_publisher_screen import AddPublisherScreen
from .author.add_author_screen import AddAuthorScreen
from .author.show_author_screen import ShowAuthorScreen

class MainScreen:
    def __init__(self, book_service, author_service, publisher_service):
        self.book_service = book_service
        self.author_service = author_service
        self.publisher_service = publisher_service

    def show_menu(self):
        while True:
            print("\n1. Add Book")
            print("2. Add Author")
            print("3. Add Publisher")
            print("4. Show Publisher and their Books")
            print("5. Show Author and their Books")
            print("6. Show Book Details")
            print("7. Show All Authors")
            print("8. Show All Publishers")
            print("9. Show All Books")
            print("10. Exit")
            choice = input("Enter your choice: ")

            if choice == "1":
                add_book_screen = AddBookScreen(self.book_service, self.author_service, self.publisher_service)
                add_book_screen.add_book()

            elif choice == "2":
                add_author = AddAuthorScreen(self.author_service)
                add_author.add_author()


            elif choice == "3":
                add_publisher = AddPublisherScreen(self.publisher_service)
                add_publisher.add_publisher()

            elif choice == "4":
                show_publisher_screen = ShowPublisherScreen(self.publisher_service)
                show_publisher_screen.show_publisher_and_book()

            elif choice == "5":
                show_author_screen = ShowAuthorScreen(self.author_service)
                show_author_screen.show_author_and_book()

            elif choice == "6":
                book_details = ShowBookScreen(self.book_service)
                book_details.show_book_details()       

            elif choice == "7":
                show_author_screen = ShowAuthorScreen(self.author_service)
                show_author_screen.show_authors()

            elif choice == "8":
                show_publisher_screen = ShowPublisherScreen(self.publisher_service)
                show_publisher_screen.show_publishers()

            elif choice == "9":
                show_books = ShowBookScreen(self.book_service)
                show_books.show_books()

            elif choice == "10":
                break

            input("Press any kez to continue")
