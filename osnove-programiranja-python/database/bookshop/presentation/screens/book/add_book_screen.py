class AddBookScreen:
    def __init__(self, book_service, author_service, publisher_service):
        self.book_service = book_service
        self.publisher_service = publisher_service
        self.author_service = author_service

    def add_book(self):
        name = input("Enter the book name: ")
        price = float(input("Enter the book price: "))
        is_available = input("Is the book available? (True/False): ").lower() == "true"

        authors = self.author_service.get_all_authors()
        for author in authors:
            print(f"{author.id}. {author.first_name} {author.last_name}")

        author_id = int(input("Enter the ID of the author: "))

        publishers = self.publisher_service.get_all_publishers()
        for publisher in publishers:
            print(f"{publisher.id}. {publisher.name}")

        publisher_id = int(input("Enter the ID of the publisher: "))

        self.book_service.create_book(name, price, is_available, author_id, publisher_id)
        print("Book added successfully!")
