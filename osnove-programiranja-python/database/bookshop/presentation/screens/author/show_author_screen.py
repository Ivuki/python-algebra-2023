class ShowAuthorScreen:
    def __init__(self, author_service):
        self.author_service = author_service

    def show_author_and_book(self):
        authors = self.author_service.get_all_authors()
        for author in authors:
            print(f"{author.id}. {author.first_name} {author.last_name}")

        author_id = int(input("Enter the ID of the author: "))

        author = self.author_service.get_author_with_books(author_id)
        if author:
            print(f"\nAuthor: {author.first_name} {author.last_name}")
            for book in author.books:
                print(f"Book: {book.name}, Price: ${book.price}")
        else:
            print("Author not found.")
    
    def show_authors(self):
        authors = self.author_service.get_all_authors()
        for author in authors:
            print(f"{author.first_name} {author.last_name}")