print("Ispisuje na ekran")
var = input('Unesite vrijednost: ') #sprema string
#jednostavni tipovi
broj_cijeli = 1 # integer number- INT
broj_decimalni = 1.1 # float - floating point number
rijeci = 'Rijeci koje zelimo ispisati' # string
evaluacijski = True # ili False -> boolean

# prebacivanje tipova
korisnikov_broj = int(input('Unesite broj: ')) # uymemo input i predamo ga int() da ga pretvori u broj
korisnikov_dec_broj = float(input('Unesite broj: ')) # uymemo input i predamo ga int() da ga pretvori u broj

# Stringovi
rijeci = 'Rijeci koje zelimo ispisati' # string
# neprovmjenjiva "Lista" znakova
nova_rijec = f'{korisnikov_broj:<15} {korisnikov_dec_broj:^15}' # formatiranje 
# "<" poravnja po pocetku "^" sredini ">" kraju
print(nova_rijec)
# listu_rijeci = nova_rijec.split(',')
print(f"{123456:09d}") # leading zero u ispisu
print(f"{3.141519:0.2f}") # zaokruyivanje


# List
#niz elemenata (vrijednosti) bilo kojeg tipa
lista = ['adsf', 'qwerty', 1, [2, 3], {'a' : 4}] # uglade
#elementi mogu ponavljati
for el in list:
    print(f'elemnt je: {el}') 
    # treba paziti na tabove 
    # ono sto je dio fora je potpisano ispod njega
    # uvuceno za 1 tab
# print(f"{rl!r}") 
lista.append('xyz') # dodaje element na kraj liste
broj_ponvaljanje_qwerty = lista.count('qwerty') # koliko puta se ponavlja qwerty
index_vrijednosti = lista.index('xzy') # index vrijednosti xyz
print(lista[0]) # dovacanje pojedinog elementa preko indexa
len(lista) # duljinu liste tj. broj elemenata
#range(10) -> generira brojeve 0 - 9 
#for i in range(10) -> iterira 10 puta
for i in range(len(lista)):
    print(f'{i}ti element je {lista[i]}')
lista.sort() # sortira listu, abecedno, ako nije drukcije zadano

matrica = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9],
]
for red in matrica:
    for element in red:
        print(element)
        element += 1

# fake liste
# #Setovi -> slicno listi ali se elementi nemogu ponavljati
set_vrijednosti = {'Jabuka', 'Banana', 'Banana'}
for el in set_vrijednosti:
    print(el) # banana se pojavljuje samo jednom
# Tuble ili ntorka -> slicno listi, ali se ne moze mijenljati ili dodavati vrijednosti
n_torka = ('Jedan', 'Dva', 'Jedan') # obler
for el in n_torka:
    print(el)
for i in range(len(n_torka)):
    print(n_torka[i]) # uglate uvijek koristimo za dohvacanje elementa neovino je li rijec o listi, setu, tuple ili dict



#DICTionary ili mapa ili hash mapa ili hash
#set parovi kljuceva i vrijednosti, di se kljucevi ne mogu, ali vrijednosti mogu ponavljati
osoba = {
    'OIB' : 123456789,
    'Ime' : "igor",
    'Prezime' : 'Vukas',
    'Pr jezici': ['Java', 'Python', 'Dart', 'Go'],
    'Neki dictionari': {'kljuc' : 'vrijednost'}
}
#kljjuc mora bit ne promjenivi tip najcesce sting ili broj
#vrijednosti moze biti bilo koji tip 
osoba['Ime'] # dohvaca preko kljuca, uvjeto indexa KORISTIMO UGLATE ZAGRADA

#iteriramo i po kljucevi i po vrijednostima
for key, value in osoba.items():
    print(f'na kljucu {key} nalayi se vrijednost {value}')

#kad hocemo samo kljuceve
for key in osoba.keys():
    print(f'kljuc {key} nalazi se vrijednost {osoba[key]}')

for key in osoba:
    print(f'kljuc {key}')

#kada hocemo samo po vrijednostima
i = 0
for value in osoba.values():
    i += 1
    print(f' {i}. vrijednosti je {value}')

osoba.update({'novi kljuc' : 'nova vrijednost'}) # i dodajemo
osoba.update({'ime' : 'Pero'}) # i mjenjavo vrijednost na postojecem kljucu
osoba.pop('ime') # izabi kljuc i odgovarajucu vrijednost
osoba.popitem() # izbaci yadnji element obicino se ne koristi
print(osoba.get('novi kljuc', 'Defoultno: ERROR NEMA TE VRIJEDNOSTI')) # slicno kao i osoba['novi kljuc'], ali nece puknut nego vratit defult u slucaju greske