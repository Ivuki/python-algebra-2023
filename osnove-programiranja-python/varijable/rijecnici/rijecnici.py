# rijecnik = {
#     'key' : 'value',
#     'key2' : 'value2'
# }
# print(rijecnik['key'])
lista_osoba = ['Igor', 'Antonio']
lista_osoba[0]
lista_osoba[1]
# for osoba in lista_osoba:
#     print(osoba)

dict_osoba = {
    '0123456789' : 'Igor',
    '5678901234' : 'Antonio'
}
dict_osoba['0123456789']
# print(dict_osoba.get('5678901234'))
for key, vaule in dict_osoba.items():
    print(f'osoba s obiom {key}\t je {vaule}')
# help(dict)
print('-------------------------------------------------------')
dict_osoba.pop('5678901234')
for key, vaule in dict_osoba.items():
    print(f'osoba s obiom {key}\t je {vaule}')
print('-------------------------------------------------------')
dict_osoba.update({'5678901234' : 'Antonio'})
for key, vaule in dict_osoba.items():
    print(f'osoba s obiom {key}\t je {vaule}')
dict_osoba.update({'5678901234' : 'Damir'})

print('-------------------------------------------------------')
for key, vaule in dict_osoba.items():
    print(f'osoba s obiom {key}\t je {vaule}')
# help(dict)


# help(dict)



osoba = {
    'ime' : 'Igor',
    'prezime' : 'Vukas',
    'zaposljenje' : 'Ima i previse',
}
osoba2 = {
    'ime' : 'Antonio',
    'prezime' : 'Pekcec',
    'zaposljenje' : 'Ima i previse',
}
osobe = {
    '1234567890' : osoba,
    '1234098765' : osoba2
}
# i = 1
# for imena in osoba['ime']:
#     print(f'{i}. ime je: {imena}')
#     i += 1
kljuceve = osobe.keys()
vrijednosti = osobe.values()
k_v = osobe.items()
# print(osobe)
# print(kljuceve)
# print(vrijednosti)
# print(k_v)
# for kljuc in kljuceve:
#     print(kljuc)
# for key, vaule in k_v:
#     print(f'Na kljucu {key} nayalzi se vrijednost:\n{vaule}')