# zapisat ljestivicu
tonovi = ['C', 'C#', 'D', 'D#', 'E', 'F', 'G', 'G#', 'A', 'A#', 'H']
tonovi += tonovi
# ton = input korisnika predpostavka da korisnik unosi postojeci ton
pocetni_ton = input('Unesite pocetni ton:   ')

tip = ''
while True:
    tip = input('Unesite tip akorda koji zelikte (za durski unesite d, za molski uneste m):    ')
    if tip == 'd':
        break
    elif tip == 'm':
        break
    else:
        print('Niste dali spravni unos molim pokusate ponovo')
# iyracunat durski akord ton, ton + 4, ton + 7
index_pt = tonovi.index(pocetni_ton)
dur_lista = ['dur'.casefold(), 'durski'.casefold(), 'd'.casefold()]
if tip.casefold() in dur_lista:
    dva = tonovi[index_pt + 4]
# izracunati molski akord: ton, ton + 3, ton + 7
else:
    dva = tonovi[index_pt + 3]
tri = tonovi[index_pt + 7]
# ispisati korisniku njegove akorde
print(f'Vas akord je: {pocetni_ton} {dva} {tri}')