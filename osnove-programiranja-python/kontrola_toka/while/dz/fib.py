#unos od korisnika za n
n = int(input('Unesite broj n:\t'))
#lista fibonachijevih brojeva
fib = [0, 1]

while True:
    #izracun sljedeceg fibbonacijevog broja
    next_fib = fib[-1] + fib[-2]
    #i dodavanje tog broj u listu
    fib.append(next_fib)
    #sve dok izracunati broj nije veci od odabranog n
    if(next_fib > n):
        break

#ispis korisniku
for el in fib[:-1]:
    print(el, end='\t')
print()