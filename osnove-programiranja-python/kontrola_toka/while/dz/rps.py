import random
w_l_d = {
    'w' : 0,
    'l' : 0,
    'd' : 0,
    't' : 0,
}
opcija = ['k', 's', 'p']
while True:
    #na random komp bira kamen skare ili papir
    komp_pick = random.choice(opcija)
    #korisnik bira sam svoj odabir (pripazit da je odabir ispravan)
    while True:
        user_pick = input('Za odabrati kamen pritisnite "k"\nZa odabrati skare pritisnite "s"\nZa odabrati papir pritisinte "p"\n').casefold()
        if user_pick not in opcija:
            print('To nije jedna od mogucnosti')
        else:
            break
    #ispis pobijda do sada (trebamo nekako pratit pobjede)
    if user_pick == komp_pick:
        print("Igra je nerjesena")
        w_l_d['d'] += 1
        w_l_d['t'] += 1
        print(f"Za sada u odigranih {w_l_d['t']} partija imate:\n {w_l_d['w']} pobijeda {w_l_d['l']} poraza {w_l_d['d']} nerijesenih")
    elif (user_pick == 'k' and komp_pick == 'p') or (user_pick == 'p' and komp_pick == 's') or (user_pick == 's' and komp_pick == 'k'):
        print("Izgubili ste")
        w_l_d['l'] += 1
        w_l_d['t'] += 1
        print(f"Za sada u odigranih {w_l_d['t']} partija imate:\n {w_l_d['w']} pobijeda {w_l_d['l']} poraza {w_l_d['d']} nerijesenih")
    else:
        print("Pobijedili ste")
        w_l_d['w'] += 1
        w_l_d['t'] += 1
        print(f"Za sada u odigranih {w_l_d['t']} partija imate:\n {w_l_d['w']} pobijeda {w_l_d['l']} poraza {w_l_d['d']} nerijesenih")
    #ponavljao dok korisnik ne kaze da mu je dosta
    again = input('Zelite li odigrati novu partiju? (y/n)')
    if again.casefold() == 'n':
        break

# lose = {
#     'k': 's',
#     's': 'p',
#     'p': 'k',
# }
# for key, val in lose:
#     if( user_pick == key) and komp_pick == val:
#         print('pobijedili ste')