# Potrebno je napraviti program za igranje igre guess the number
# Igra ima 2 nacina rada 
# Mode 1: Racunalo "zamisli" broj izmedu 1-100, 
# a korisnik ga mora pogoditi 
# na kraju svakog pokusaja racunalo ispise je li zamisljen broj veci ili manji

# Mode 2: Igrac zamisli broj, a program ga mora pogoditi
# na kraju svakog pokusaja igrac racunalu napise je li zamisljen broj veci ili manji

# Opcija 3: Ispis pravila igra -> 
# korisniku se samo ispisuju pravila igre za pojedine modove

# Opcija 4: prekid igre

# Meni: mora ispisati opcije okrisniku 
# to se mora desiti na kraju svake igre
import random

def menu():
    print('''
    1. Guess the number
    2. Pick a number
    3. Game rules 
    4. Exit game
    ''')
    user_pick = 0
    while user_pick not in list(range(1,5)):
        user_pick = int(input('Pick a menu option:\t'))

    return user_pick

def guess_the_number():
    pc_number = random.randint(1, 100)
    while True:
        user_guess = int(input('Pick a number between 1 and 100:\t'))
        if user_guess == pc_number:
            input(f'Congrats {user_guess} is my number! You have won!')
            return # finish end exit when the number is guesset
        elif user_guess > pc_number:
            print('To high, my number is lower!')
        else:
            print('To low, my number is higer than that!')
    return

def user_feedback(number):
    choises = ('g', 'l', 'e')
    while True:
        user_feedback = input(f'''
            Is your number: 
            (g)reater than {number}, 
            (l)ower than {number},
            (e)queal to {number},
            '''
        )
        if user_feedback in choises:
            return user_feedback
        else:
            print('Upss, that\'s not any of the options')
    return

def pick_a_number():
    input("Imagine a number between 1 and 100, and press ENTER when you are ready")
    tryed = [50]
    pc_pick = 50
    move_by = 25
    while True:
        user_feed = user_feedback(pc_pick)
        if user_feed == 'g':
            pc_pick += move_by
        elif user_feed == 'l':
            pc_pick -= move_by
        else:
            print("GOT YA! Your number is {pc_pick}")
            return
        move_by = 1 if move_by // 2 == 0 else move_by // 2
        if pc_pick in tryed:
            print("HEEEY! You cheated! I not gonna play like that")
            return
        tryed.append(pc_pick)
    return 

def show_rules():
    input('''
        MODE 1: I pick a number between 1 and 100. Each round you as the player,
        give me a number between 1 and 100 as a guess, and I tell you is my number 
        greater lower or equal to your number.

        MODE 2: You as the player imagine a number between 1 and 100, and I try to guess it
        each round you tell me if your number is greater, lower or equal to my number

        MODE 3: You are here now these are the rules

        MODE 4: By pressing 4 you can finish the game!

        NOTE: press ENTER when done reading
    ''')

while True:
    user_pick = menu()
    if user_pick == 1:
        guess_the_number()
    elif user_pick == 2:
        pick_a_number()
    elif user_pick == 3:
        show_rules()
    elif user_pick == 4:
        print("Adios amigos!")
        break
    else:
        print("Hmmm, something is going on here")
