# 
#? Napraviti igru krizic kruzic u kojoj 2 igraca naizmjenicno biraju polje
#? u koje zele unijeti svoj znak numerirani s 1-9, te ako je moguce se znak unosi.
#? Ako nije moguce unjeti znak na to polje isti igrac ostaje na potezu,
#? ali uz poruku o gresci. Na kraju svake partije se ispisuje korisnima poruka,
#? to je pobijedio (ili ako je igra bila nerijesena). 
#? Igraci se nakon toga pitaju, ako zele zapoceti novu igru ili ne.

### PULS ZADATAK: pracenje ukupnog rezultata (tko je pobijedio vise). 

### PULS PLUS ZADATAK: ScoreBoard -> 
### gdije se za svakog igraca prati koliko ima pobijeda, podaza ili nerijesenjih

# !HELP: (napisite funkcije)

#cosntants
win = 'w'
lose = 'l'
draw = 'd'
X = 'X'
O = 'O'
# TODO: Prikaz ploce: koja ispisuje trenutno stanje ploce u 3X3 formatu
def show_board(board):
    for row in range(len(board)):
        print('---------------')
        for col in range(len(board[row])):
            print(f'| {board[row][col]} |', end= '')
        print()
        print('---------------')
# TODO: Odredivane znaka: koja odreduje tko je trenutno na potezu (X ili O)
def toggle_sing(sing):
    if sing == X:
        return O
    return X
# TODO: Upis znaka: koja provjeravamo moze li se upisati znak,
# ako da upisuje vrijednosti X ili O na trazeno polje
def add_value(sing, board):
    # check is valid
    while True:
        position = int(input('Chose a location:\t'))
        row = (position - 1) // 3
        col = (position - 1) % 3
        if board[row][col].isdigit():
            board[row][col] = sing
            return board
        else:
            print("That position is taken")


# TODO: Provjera pobijede: koja provjerava horizontale, vertikale i dijagonale
# te vraca postoji li pobijednik ili ne
def check_hrl(board):
    if board[0][0] == board[0][1] == board[0][2]:
        return True
    if board[1][0] == board[1][1] == board[1][2]:
        return True
    if board[2][0] == board[2][1] == board[2][2]:
        return True
    return False

def check_vrl(board):
    if board[0][0] == board[1][0] == board[2][0]:
        return True
    if board[0][1] == board[1][1] == board[2][1]:
        return True
    if board[0][2] == board[1][2] == board[2][2]:
        return True
    return False

def check_dgl(board):
    if board[0][0] == board[1][1] == board[2][2]:
        return True
    if board[0][2] == board[1][1] == board[2][0]:
        return True
    return False

def check_win(sign, board):
    if (check_hrl(board) or check_vrl(board) or check_dgl(board)):
        print(f'{sign} has won this game!')
        return True
    return False

# TODO: Provjera kraja: kada su sva polja popunjena
def check_draw(board):
    for row in board:
        for el in row:
            if el.isdigit():
                return False
    print('This game is a draw')
    return True

def start_game():
    board = [
        ['1', '2', '3',],
        ['4', '5', '6',],
        ['7', '8', '9',],
    ]
    sign = X
    player1 = input("Enter user 1 name:\t")
    player2 = input("Enter user 2 name:\t")

    return board, sign, player1, player2

def set_players(player1, player2, score):
    if player1 not in score.keys():
        score.update(
            {
                player1: 
                {
                    win: 0,
                    lose: 0,
                    draw: 0,
                },
            },
        )    
    if player2 not in score.keys():
        score.update(
            {
                player2: 
                {
                    win: 0,
                    lose: 0,
                    draw: 0,
                },
            },
        )

    return score

def update_score(score, player1, player2, chr):
    if chr == draw:
        # read score
        player1_score = score[player1]
        player2_score = score[player2]
        # update score
        player1_score[draw] += 1
        player2_score[draw] += 1
        # write score
        score[player1] = player1_score
        score[player2] = player2_score
        return 
    if chr == X: # player 1 won
        # read score
        player1_score = score[player1]
        player2_score = score[player2]
        # update score
        player1_score[win] += 1
        player2_score[lose] += 1
        # write score
        score[player1] = player1_score
        score[player2] = player2_score
        return
        # read score
    player1_score = score[player1]
    player2_score = score[player2]
    # update score
    player1_score[lose] += 1
    player2_score[win] += 1
    # write score
    score[player1] = player1_score
    score[player2] = player2_score
    return 

def show_scoreboard(score):
    print(score)
    for key in score.keys():
        for val in score[key].values():
            print(val, end='  ')
        print()

def main():
    score = {}
    while True:
        board, sign, player1, player2 = start_game()
        score = set_players(player1, player2, score)
        while True:
            show_board(board)
            board = add_value(sign, board)
            if check_win(sign, board):
                update_score(score, player1, player2, sign)
                break
            if check_draw(board):
                update_score(score, player1, player2, draw)
                break
            sign = toggle_sing(sign)

        show_score = input("Do you wan't to see the scoreboard?(y/n)\t")
        if show_score == 'y':
            show_scoreboard(score)

        user_input = input("Do you wan't to play a new game?(y/n)")
        if user_input == 'n':
            break

        

if __name__ == "__main__":
    main()