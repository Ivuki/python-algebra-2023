import json

# class Person():
#     def __init__(self) -> None:
#         pass
#     def toJson(self):
#         dicta = {
#             'Ime': self.ime
#         }
#         return dicta
        
#     def fromJson(self, json):
#         return self.__init__()

# citanje iz json
try:
    with open('write_data.json', 'r') as file:
        json_dict = json.load(file.read())
except Exception as e:
    print(f'{e}')

# pisanje u json
try:
    with open('write_data.json', 'a') as file:
        dicta = {
            'ime': "igor"
        }
        json.dump(dicta, file, indent= 4)
except Exception as e:
    print(f'{e}')
