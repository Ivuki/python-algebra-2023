

# otvorimo
file =  open('ime.txt', 'r') # open ime, tj. putanja do file-a i nacin za otvorit r/w
# citamo 
data = file.read() # cijeli file u string
# file.readlines() # daje listu red po red
# zatvorimo
file.close()

# otvorimo
file = open('ime.txt', 'w')
file = open('ime.txt', 'a') # a apend pise na kraj file-a umjesto da prebrise file svaki put
# pisemo 
ime = 'NEko ime'
file.write(ime)

# zatvorimo
file. close()

#PReporucljivije
with open('ime.txt', 'r') as file:
    pass

#neka komanda

# najbolji nacin rada s datotekama
try:
    with open('ime.txt', 'r') as file:
        print(file)
except Exception as e:
    print(f"desio se error {e}")

# ERRRORI TRY-EXCEPT

class MyError(Exception):
    pass

try:
    # naredbe koje su nesigurne
    file = open('ime', 'w')
    if(file != "Neki tamo"):
        raise MyError
except MyError as me:
    print('File mora biti Neki tamo')
except ZeroDivisionError as ze:
    print('Ne mozes dijeliti s nulom, tko te matematuku ucio')
except Exception as e:
    print(f'Dogidila se greska {e}')
finally:
    file.close()
