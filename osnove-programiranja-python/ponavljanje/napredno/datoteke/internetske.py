import requests
import json

URL = 'https://jsonplaceholder.typicode.com/todos'
# link na API

try:
    data = requests.get(URL)
    with open('dta.json', 'w') as file:
        json.dump(data, file)
except Exception as e:
    print(e)
# GET -> dohvatit
# POST -> add new
# PUT -> update existing
# PATCH -> updated existing part
# DELETE -> delete elemnt