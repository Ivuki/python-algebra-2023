import tkinter as tk
# PyQt -> slozeniji, ali ima vise mogucnosti, bolji je crosover izmedu jeznika jer ima verziju i za C++, js itd. 

# wigdet -> jedan graficki element
# window -> ekran tj gravni prozor koji sadryi sve ostale elemente 
# frame -> organizacijska cijelina unutar window-a (HTML div) sluzi za grupiranje elemenata 
# gemterija -> pack, grid i place; odreduju di ce sta stajat na ekranu
# Event drivern programing -> umjesto direktne provjerene je li se dogodio event, element sam javlja kada se kod njega dogodila promjena
# Callbacks -> funkcija koja se poziva na neki element

# postavimo glavni prozor
root = tk.Tk()
root.title('GUI')
root.geometry('600x800')

# dodamo elemente u prozor
button = tk.Button(root, text='Kliki')
#tk.Label()
# tk.PhotoImage()
# tk.Entry()
# tk.Checkbutton()
# tk.Radiobutton()
# tk.Listbox()
# tk.Menu()
# tk.Menubutton()
# tk.Message() otvori novi prozor za ispit te poruke
button.pack()

# pokrenemo prikaz
root.mainloop()


#GEOMETRIJE
# button.pack() -> elementi se neriraju jedan ispod drugog, cesto pre simplificirano
# button.place(x=100, y=50) -> precino u pixel postavlja svaki element, obicno naporno za slagat
# button.grid(row=0, column=0) -> najcesce koristen, mix predhodnda dva, 
# daje slozenije sucenje bez posebne zezancije tocnog pixela i veliceine svakog elementa 


#EVENTI
# odaberemo enet iz liste -> googlajte ovo su samo neki
# <Button-1> -> mouse button lijevi
# <ButtonRelease-1>
# <Enter>
# <FocusIn>
# <Return>
# a -> za slovo a
# <Key> -> bilo sta da se stisni
# ....


def handle_key(event): # mora primati enet koji ju je pozvao
    labela_var.set(event.char)

root = tk.Tk()
root.title('GUI')
root.geometry('600x800')

# dodamo elemente u prozor
labela_var = tk.StringVar()
labela_var.set('adsfwr')
labela = tk.Label(root, textvariable=labela_var)

labela.pack()

root.bind('<Key>', handle_key) #-> blind spaja event i funkciju (callback)
root.mainloop()