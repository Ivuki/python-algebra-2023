import abc

# class Hellper():
#     def getScreenSize():
#         pass

# class Sizes():
#     screeSize = 50
#     smallScreen = 25


class FooClass(abc.ABC):
    def __init__(self, name):
        self.id = self.__generateId()
        self.name = name

    def __generateId(self):
        pass

    def sayHi(self):
        print(f"HI {self.name}")

    def staticnaFja():
        print("Pozdrav svima")

class FooDjete(FooClass):
    def __init__(self,name):
        self.id = self.__generateId()

    def __generateId(self):
        return "123456"
        # return super().__generateId()

class FooUnuk(FooDjete):
    def __init__(self, name):
        super().__init__(name)
    
    def sayHi(self):
        print(f"Hello {self.name}")

foo = FooClass("Igor") # foo objekt tipa FooClass
foo.sayHi()

foo2 = FooDjete('Vokas')
foo2.sayHi()

foo3 = FooUnuk('adfa')
foo3.sayHi()
# FooClass.staticnaFja() # staticna f-ja nkad se koristi za organizaciju, anije bas cesto u pythonu koliko u javi i C#
# Hellper.getScreenSize()
# Sizes.screeSize

# Enkapsulaciju -> sve vezano za neku klasu tj tip podatka je odmah tamo i posloyeno clean code
# POlimorfizam -> razlicite implementacije tretiramo kao iste
# Nasljedivanje -> jedna klasa moze nasljediti drugi i onda napraviti izmene