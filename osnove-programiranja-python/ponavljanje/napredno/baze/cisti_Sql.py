import sqlite3

try:
    # spojimo se na bazu
    sqlConection = sqlite3.connect('ImeBaze.db')
    cursor = sqlConection.cursor()

    # napravimo query
    q = 'SELECT * FROM users'

    # izvrsimo querry
    cursor.execute(q)

    sqlConection.commit() # kada pisemo podatke

    # zatvorimo konekciju
    cursor.close()
except Exception as e:
    print(e)

# QUERRY mora biti SQL i onda ubacen u string
# read
'''
    SELCET what 
    FROM table
    WHERE uvijet
'''
#create
'''
    INSERT INTO table(what)
    WALUES(?) 
'''
# upitnik mora biti tu, kasniej se korz cursor.exectue dodaje
# cursor.execute(q, (what))
#updated
'''
    UPDATE table
    SET what = ?
    WHERE uvijet
'''
#detele
'''
    DELETE FROM table
    WHERE id = ? 
'''