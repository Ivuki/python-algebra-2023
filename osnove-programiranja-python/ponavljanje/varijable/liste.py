#LISTE CRUD (creat, read, update, delete)
lista = ["podatak_1", 'podatak_2']
print(lista)
lista.append('novi element') #ubacuje na kraj
lista.insert(15, 'vrijednost') #ubacuje na index
print(lista)
prvi_el = lista[0] #broj indexa 0 -> je prvi index
print(prvi_el)
lista[0] = 'Nova vrijednost'
print(lista)
i = lista.index('novi element') # vraca indext na kojem je taj element
print(i)
lista.pop(0) # vrati element na tom indexu i obrise ga iz liste
lista.remove('novi element') # brise, ali vrijedno -> lista.pop(lista.index('novi_elemnt))
lista.sort()
print(lista)

matrica = [
    [0, 1, 2],
    [10, 11, 12],
    [20, 21, 22],
]
print(matrica[0][0])

# lista = [1, 2, 3, 4]
# print(lista)
# zadnji_el = lista.pop() # obrise i spremi obirisani u zadnji_el
# print(lista)
# print(lista.pop(0))
# print(lista)
