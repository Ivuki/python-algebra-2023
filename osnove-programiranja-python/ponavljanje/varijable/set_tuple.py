# SET "lista" u kojoj se ne ponavljaju vrijednosti
# set je neured -> nema garancije sta je na kojem mijestu
# ima CRUD
prazni_set = set() #mora ovako
nas_set = {} # ovo je prazni dict

puni_set = {6, 2, 3, 4} #moze ovako kad ima vrijednost
print(puni_set)

puni_set.add(4) # nema ponavljanja
puni_set.add(5) # doda novi vrijednost
print(puni_set)

print(puni_set.pop()) # izbacuje uvije prvi
print(puni_set)
print(puni_set.remove(3)) # brijese poslani element
print(puni_set)

# TUPLE "lsita" podataka koji se ne mijenjaju
# nema crud (nema upodat-a i mena deleta), samo read i create
nas_tuple = ('prvi', 'drugi', 'treci')
print(nas_tuple)
print(nas_tuple[0])