# Zapis podataka
# zapis u varijable
# jednostavniji tipovi -> Broj (float, int), text(string)... {bool}
# slozeni tipovi -> 
# liste -> puno nekih posdataka, u pythonu ne moraju biti svi istog tipa, ali obicno jesu
# dict -> kada nam je potrebno imenovati stvari kljuc-value parove
# set -> kada zelimo osigurati da je svaki element razlict 
# tuple (kad ne zelimo mijenjati niti jedna element)
# konstante -> varijabla koju ne zelimo mijenjati
KLJUC = 'kljuc'

# BROJEVI
cijeli_broj_1 = int(5) # nemaju decamalnu tocku
cijeli_broj_dva = int(7)
zbroj = cijeli_broj_1 + cijeli_broj_dva # zbroj int
razlika = cijeli_broj_1 - cijeli_broj_dva # razlika int
mnozenje = cijeli_broj_1 * cijeli_broj_dva # umnozak int
dijeljenje = cijeli_broj_1 / cijeli_broj_dva # float
cijelobrojno_dijenjenje = cijeli_broj_1 // cijeli_broj_dva # odrezani int (ignorirat ce sve iza deimalne tocke)
ostatak_pri_dijenjenju = cijeli_broj_1 % cijeli_broj_dva # ostatak -> gledamo ciji je potez runda % 2 (0-1) if 0 igrac_1, else igrac_2
pocenticiranje = cijeli_broj_1 ** cijeli_broj_dva # a^b prvi broj na drugi broj

dec_br_1 = float(5.0)
dec_br_2 = float(7.0)
zbroj = dec_br_1 + dec_br_2 # zbroj float
razlika = dec_br_1 - dec_br_2 # razlika float
mnozenje = dec_br_1 * dec_br_2 # umnozak float
dijeljenje = dec_br_1 / dec_br_2 # float

# TEKS -> string (lista znakova koja se ne moye mijenjati naknadno)
string = str('nekoliko rijeci')
string_dva = string.capitalize()
# print(string_dva)
string.casefold() # da forsa sva slova u mala -> obicno kod kod nekih provjera
string.isalnum() # da su svi ili ili slova ili brojevi
string.isalpha() # da su svi slova
lista_rijeci = string.split(' ') # podjelit string i dobit listu (dijeli po polanom znaku) 
zbroj = string_dva + string # daje string gdje attacha jedno na drugo
lista_rijci = input("Unesite nekoliko rijeci odvojenje razmakom:\t").split()
print(lista_rijci[0])
# print(string)
# print(string + str(5))
# print(string * 5)
# . . .
# Obrada podataka