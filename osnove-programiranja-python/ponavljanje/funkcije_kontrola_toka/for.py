# FOR -> kad zelimo da se nesto izvrski tocno odreeni broj puta
def ispis_novi_for():
    print('Novi for')

def ispis_u_foru():
    print('U for petlji sam')
    for j in range(5):
        ispis_novi_for()

for i in range(3):
    ispis_u_foru()

lista = [1,2,3,4,5,6,7]

for i in range(len(lista)):
    print(lista[i], end=' ')
print('\n',end='')

for i in lista:
    print(i, end=' ')
print(end='\n')
# print('Moj string', end=' Ovo je na kraju\n')
# print(f'{"Moj string" + " Ovo je na kraju"}', end='\n')
i = 0
while i < len(lista):
    print(lista[i], end= ' ')
    i += 1
print()

matrica = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9],
]
for i in range(len(matrica)):
    for j in range(len(matrica[i])):
        print(matrica[i][j], end=' ')
    print()
# for j in range(len(matrica[0])):
#     print(matrica[0][j], end=' ')
# print()
# prvi_red = matrica[0]
# print(prvi_red[0])
# print(matrica[0][0])