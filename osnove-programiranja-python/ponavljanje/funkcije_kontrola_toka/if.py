def foo_prvi_broj_je_veci():
    print('Prvi broj je vec')

def foo_prvi_broj_je_manji():
    print('Drugi broj je vec')

def foo_brojevi_su_jednaki():
    print('Brojevi su jednaki')

a = int(input())
b = int(input())
if a > b:
    #block naredbi koji se izvrsava ako je uvijet istinit
    foo_prvi_broj_je_veci()
elif a < b:
    # SAMO ako prvi uvijet nije zadovoljen proverava se drugo
    foo_prvi_broj_je_manji()
else:
    #ako niti jedan uvijet nije bio zadovolje izvrsava se ovaj blok naredbi
    foo_brojevi_su_jednaki()
