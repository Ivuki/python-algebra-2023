# napraviti klasu auto
class Car():
# parametri koje mora imati
# obujam motora
# snaga moora
# maksimalan bryina (max je 200) ne smije biti negativni broj
# boja
# proiyvodac
# model
# godiste
# vozno stanje (ne vozi, lose, ok, dobro, odlicno)

    def __init__(
        self,
        motor_power_hp,
        max_speed,
        color, 
        manifact,
        model, 
        year_of_creation,
        motor_cc = 1000,
    ):
        self._motor_volume = motor_cc
        self._hp = motor_power_hp
        self._max_speed = self.set_max_speed(max_speed)
        self._color = color
        self._manifact =  manifact
        self._model = model
        self._year = year_of_creation
        self._is_driveable = True



    def set_max_speed(self, max_speed):
        if max_speed < 0:
            self._max_speed = 0
        elif max_speed > 200:
            self._max_speed = 200
        else:
            self._max_speed = max_speed
        return self._max_speed
    # def get_motor_volume(self):
    #     return self._motor_volute

    # def set_motor_volume(self, motor_volume):
    #     if motor_volume <= 0:
    #         print('error')
    #         return
    #     self._motor_volume = motor_volume

# moye se:
# vooyit odredenom bryinom (do maksimalne) pri pozivu se salje kojom to brzinom
    def drive_at_speed(self, speed):
        print(f"Driveing at speed {speed}")
# moze se zaustavit
    def stop_driveng(self):
        print(f'The car {self._manifact} {self._model} has stopped')
# pokvario se postavlja stanje u ne vozi
    def brooken(self):
        print("The car is brooken")
        self._is_driveable = False
# popravi se postavlja stanje u odlicno
    def fix_car(self):
        print("The car was fixed and is drivable")
        self._is_driveable = True

    def to_string(self):
        print(f''' 
        The car {self._manifact} {self._model}
        is {self._color} and form the year {self._year}
        it has a {self._motor_volume} cc engine,
        that produces {self._hp} HP,
        and goes at max speed of {self._max_speed}
        currently it is {'drivable' if self._is_driveable else 'not drivable'}
        ''')

# parametri se unose preko inputa VANI, a proslijedite =ih u klasu

# opigrajte se s metodama

cc = int(input("Unesite volumen motora\t"))
hp = int(input('Unesite snagu motora\t'))
igorov_auto = Car(
    hp, 
    150,
    "black",
    'Honda',
    'Civic',
    1999,
)

igorov_auto.to_string()

igorov_auto.brooken()

igorov_auto.to_string()

# OOP postoji -> public i private (klase, varijable i metode)

# GLOBALNE VARIJABLE
# def foo(varijabla):
#     varijabla  = varijabla + 50
#     return varijabla

# def foo2(varijabla):
#     varijabla = varijabla * 2
#     return varijabla


# def main():
#     varijabla = 199

#     foo(varijabla)
#     foo2(varijabla)
# INPUT UNUTAR FUNKCIJE ZA OBRADU

racuni = {}

def uplata_na_racun(racun, suma):
    racuni[racun]['amount'] += suma

racun = input("unesite broj racuna")
while True:
    suma = input('Unesite depozit')
    if suma > 0 and suma < 1000:
        break

uplata_na_racun(racun, suma)

# arhitektura  M-V-C
# model class bankovni_racun
# view input(unesite vrijednost depozita)
# contorler def obrada_depozita(depozit) -> sve provjere unosa 
# i on bi pozvao model (bankovni_racun) da spremi vrijednost, 
# ya bazu pozivali unutar conttolera 
# napomena odvojena klasa ya baratanje 
# unos korisinka (view UI), obrada podataka (servisi ili contoler), spramanje u bazu (db sloj) + model (koji opisuje kako izgleda clasa ya unos)
# baya javi uspijesno spramnje controleru (servisu), servis vraca UI -> ispisuje na ekran
# model na ekranu je da ne 
# model u bazi ima 15 maparamatara -> na osnovi iyracunajs dali biljka dobro raste
# bd_model (kako ce izgeldat u bazi)a drugo je entitet (kako ce izlgledat na ekranu)
# model layer -> Biljka_db_model, Biljau_ui_model
# data acsses layer -> povuce iz baze vrati Biljak_db_model
# controler (servis leyeru) -> biljka_db_model pretvori u Biljka_ui_model
# view (UI layer) -> prikayao korisniku BUljka_ui_model ali formatirano