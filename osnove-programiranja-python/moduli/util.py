import os
from random import randint
import datetime as dt

def cls():
    os.system('cls' if os.name=='nt' else 'clear')

def get_random_int():
    return randint(1, 150)

def get_current_time():
    return dt.datetime.now().strftime('%H : %M : %S')