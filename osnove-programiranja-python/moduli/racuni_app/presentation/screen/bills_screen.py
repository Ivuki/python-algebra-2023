from ...model.bill import Bill
import util as u

def get_new_bill():
    return Bill()

def add_artical(bill : Bill):
    name = input("Add artical name")
    price = input("Add artical price")
    bill.add_artical(name= name, price= int(price))
    return bill

def remove_artica(bill : Bill):
    name = input("Add name to remove")
    bill.remove_artical_by_name(name= name)
    return bill

def print_bill(bill : Bill):
    print(f'''
    -------- ZAGLAVLJE -------------
    ID:{bill.id}    DATE:{bill.date}
    ''')
    for el in bill.articals:
        print(f'''
    {el.name}      {el.price}
        ''')
    print('''
    ------------ FOTTER -------------
    ''')
def meni():
    while True:
        input('ENTER')
        u.cls()
        user_input = input('''
        1. CREATE NEW BILL
        2. ADD NEW ARTICAL TO LAST CREATED BILL
        3. REMOVE ARTICAL FORM LAST CREATED BILL
        4. PRINT LAST BILL
        5. EXIT
        ''')
        if user_input == '1':
            bill = get_new_bill()
        elif user_input == '2':
            bill = add_artical(bill= bill)
        elif user_input == '3':
            bill = remove_artica(bill= bill)
        elif user_input == '4':
            print_bill(bill= bill)
        elif user_input == '5':
            exit()
        else:
            print('not a valid input')