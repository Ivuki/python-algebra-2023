from datetime import datetime as dt
from . import artical as a

class Bill():
    __number : int = 0
    def __init__(self) -> None:
        
        self.date = dt.now().strftime('%d.%m.%Y.')
        self.id = self._generate_id()
        self.articals = []

    def _generate_id(self):
        Bill.__number += 1
        _id = self.date
        _id += str(Bill.__number).zfill(5)
        return _id

    def add_artical(self, name : str, price : int):
        _art = a.Artical(price = price, name = name)
        self.articals.append(_art)

    def remove_artical_by_name(self, name : str):
        nl = []
        for el in self.articals:
            if el.name != name:
                nl.append(el)
                
        self.articals = nl
