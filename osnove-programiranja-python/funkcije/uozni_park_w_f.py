def unos_vozila():
    tip = input('Unesite tip vozima: ')
    proizvodac = input("Unesite proizvodaca: ")
    rega = input("Unesite registraciju: ")
    pr = input("Godinu prve registracije: ")
    cj = input("cijeku u eurima: ")
    temp_vozilo = {
        'id' : i,
        'tip' : tip,
        'proizvodac' : proizvodac, 
        'rega' : rega,
        'prva reg.' : pr,
        'cijena' : cj,
    }
    return temp_vozilo

def ispis_w_heders(vozila):
    #za ispis nam treba zaglavlje
    headers = ('ID', 'Tip', 'Proizvodac', 'Rega', 'God. pr.', 'Cijena')
    # print(vozila[1]['proizvodac'])
    for i in headers:
        print(f'{i:^15}', end=' ')
    print()
    print('------------------------------------------------------------------------------------------------------------')
    for vozilo in vozila.values():
        for key, value in vozilo.items(): 
            print(f'{value:^15}', end=' ')
        print()



vozila = {}
i = 0
while True:
    i += 1
    temp_vozilo = unos_vozila()
    vozila.update({i : temp_vozilo})
    dalje = input('zelite li unjeti novo vozilo(y/n):\t')
    if(dalje.casefold() == 'n'):
        break

ispis_w_heders(vozila)