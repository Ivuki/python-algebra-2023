from sqlalchemy.orm import sessionmaker
from models.user import User
from typing import List

class UserService:
    def __init__(self, engine):
        self.engine = engine
        self.Session = sessionmaker(bind=self.engine)
    
    def get_all_users(self) -> List[User]:
        sesssion = self.Session()
        return sesssion.query(User).all()


    def get_user_by_name(self, f_name, l_name) -> User:
        session =  self.Session()
        return session.query(User).filter(User.first_name == f_name and User.last_name == l_name).first()
    
    def get_user_by_pin(self, pin: int) -> User:
        session = self.Session()
        return session.query(User).filter(User.pin == pin).first()

    def add_user(self, f_name, l_name, pin, is_active) -> bool:
        session = self.Session()
        u = User(
            first_name = f_name,
            last_name = l_name,
            pin= pin,
            is_active = is_active
        )
        session.add(u)
        session.commit()
        return True

    def delete_by_id(self, id) -> bool:
        session = self.Session()
        u = session.query(User).filter(User.id == id).first()
        session.delete(u)
        session.commit()
        return True
    def add_admin(self) -> None:
        #check if has admin
        session = self.Session()
        admin = session.query(User).filter(User.first_name == "ADMIN").first()
        #if no admin add one
        if(admin == None):
            a = User(
                first_name = "ADMIN",
                last_name = "",
                pin= 1234,
                is_active = True
            )
            session.add(a)
            session.commit()
        return