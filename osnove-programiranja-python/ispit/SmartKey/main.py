from sqlalchemy import create_engine
from models.base import Base
from services.user_service import UserService
from presentation.screens import SmartKeyApp

DB_NAME = 'SmartKey.db'
DATABASE_URL = f'sqlite:///{DB_NAME}'

def main():
    engine = create_engine(DATABASE_URL)
    Base.metadata.create_all(engine)

    user_service = UserService(engine)
    user_service.add_admin()
    
    app = SmartKeyApp(user_service= user_service)
    app.run()


if __name__ == '__main__':
    main()

