import tkinter as tk

root = tk.Tk()
root.title('Naslov')
root.geometry('600x400')

btn = tk.Button(
    root,
    text='Text gumba'
)

lbl = tk.Label(
    root,
    text='Labela'
)

# GEO 1. pack -> jednostavno horzontalni ili vertikalno slaganje elemetanta
# btn.pack()
# lbl.pack()

# GEO 2. place super za precizno postavljanje, minus je kad resizamo i tocno nac lokaciju koja pase
btn.place(
    x=150, 
    y=100
)
lbl.place(
    x=150,
    y=300
)

root.mainloop()