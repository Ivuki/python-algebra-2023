import tkinter as tk

unsena_slova = ''
def on_keypress(event):
    print(event.char)
    global unsena_slova
    unsena_slova += str(event.char)
    labela.set(unsena_slova)



root = tk.Tk()
root.title('Naslov')

labela = tk.StringVar()
labela.set('Prikaz slova')

labela_naslov = tk.Label(
    root,
    text='Key event',
    font=('Segoe UI', 18) 
)

labela_ispis = tk.Label(
    root,
    textvariable=labela,
    font=('Segoe UI', 24),
    fg='red'
    )

text = tk.Text(
    root,
)

labela_naslov.grid(
    column= 0, 
    row= 0
)

labela_ispis.grid(
    column= 0,
    row= 1
)
text.grid(
    column=2,
    row=2
)

root.bind('<Key>', on_keypress)

root.mainloop()