from sense_emu import SenseHat

sense = SenseHat()

red = (255,0,0) # RED, GREEN, BLUE 0-255
green = (0,255,0)
blue = (0,0, 255)
white = (255, 255, 255)

while True:
    temp = sense.get_temperature()
    colour = white
    if temp < 10:
        colour = blue
    elif temp > 45:
        colour  = red
    else:
        colour = green

    msg = f'{temp}'
    sense.show_message(msg, scroll_speed= 0.1, text_colour=colour, back_colour=white)