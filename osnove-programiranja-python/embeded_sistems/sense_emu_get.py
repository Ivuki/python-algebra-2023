from sense_emu import SenseHat

sense = SenseHat()

temp = sense.get_temperature()
vlaga = sense.get_humidity()
pritisak = sense.get_pressure()
# sense.get_compass_raw
# sense.get_gyroscope_raw
# sense.get_accelerometer

print(temp)
print(vlaga)
print(pritisak)